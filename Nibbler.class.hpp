#ifndef NIBBLER_CLASS_HPP
# define NIBBLER_CLASS_HPP

#include <string>
#include <iostream>
#include <list>
#include <vector>
#include "NibblerObject.class.hpp"
#include "INibbler.class.hpp"

class NibblerObject;
class INibbler;

class Nibbler
{
public:
	Nibbler();
	Nibbler(Nibbler const & src);
	virtual ~Nibbler();
	Nibbler &	operator=(Nibbler const & rhs);
	std::string	toString() const;
	std::list<NibblerObject *>		getList() const;
	void	setList(std::list<NibblerObject *> &val);
	std::list<NibblerObject *>		getFood() const;
	void	setFood(std::list<NibblerObject *> &val);
	int		getX() const;
	void	setX(int &val);
	int		getY() const;
	void	setY(int &val);
	void	push(NibblerObject * val);
	void	pushFood(NibblerObject * val);
	NibblerObject	*findObject(int x, int y);
	NibblerObject 	*findFood(int x, int y);
	void	move();
	void	turnLeft();
	void	turnRight();
	INibbler*	getLib();
	void	setLib(INibbler* &val);
	std::list<std::vector<int> >	gameInfo();


private:
	std::list<NibblerObject *>	_list;
	std::list<NibblerObject *>	_food;
	INibbler *					_lib;
	int							_x;
	int							_y;
};

#endif
