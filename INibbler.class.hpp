#ifndef INIBBLER_CLASS_HPP
# define INIBBLER_CLASS_HPP

#include <string>
#include <iostream>
#include <list>
#include <vector>

class INibbler
{
public:
	virtual	~INibbler() {}
	virtual void	init() = 0;
	virtual	char	getInput() = 0;
	virtual void	libraryLoop() = 0;
	virtual void	printGame(int x, int y, std::list<std::vector<int> > game) = 0;
	virtual void	end(int x, int y, std::list<std::vector<int> > game) = 0;
};

#endif
