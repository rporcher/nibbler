#ifndef ANCURSES_HEADER
#define ANCURSES_HEADER
#include <string>
#include <iostream>
#include <INibbler.class.hpp>

class ANcurses: public INibbler
{
public:
	ANcurses();
	ANcurses(ANcurses const &);
	virtual ~ANcurses();
	std::string	toString() const;
	ANcurses&	operator=(ANcurses const &);
	void	init();
	char	getInput();
	void	libraryLoop();
	void	printGame(int x, int y, std::list<std::vector<int> > game);
	void	end(int x, int y, std::list<std::vector<int> > game);
};

extern "C" 
{
	ANcurses	*createLib();
	void		deleteLib(INibbler *lib);
}

std::ostream	&operator<<(std::ostream &o, ANcurses const &i);
#endif /*ANCURSES_HEADER*/
