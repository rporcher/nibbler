#include "ANcurses.class.hpp"
#include <ncurses.h>
#include <unistd.h>

ANcurses::ANcurses()
{
}
ANcurses::ANcurses(ANcurses const &model)
{
	*this = model;
}
ANcurses::~ANcurses()
{
}
std::string	ANcurses::toString() const
{
	return "ANcurses";
}
ANcurses&	ANcurses::operator=(ANcurses const &copy)
{
	(void)copy;

	return *this;
}
std::ostream	&operator<<(std::ostream &o, ANcurses const &i)
{
	o << i.toString();
	return o;
}

void	ANcurses::init()
{
	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	curs_set(0);
}

char	ANcurses::getInput()
{
	int		c;

	c = getch();
	if(c == KEY_LEFT)
		return 'l';
	if(c == KEY_RIGHT)
		return 'r';
	if (c == 49)
		return '1';
	if (c == 50)
		return '2';
	return 'n';
}

void	ANcurses::libraryLoop()
{
	usleep(150000);
	timeout(0);
}

void	ANcurses::end(int x, int y, std::list<std::vector<int> > game)
{
	this->printGame(x, y, game);
	timeout(-1);
	getch();
	endwin();
}

void	ANcurses::printGame(int x, int y, std::list<std::vector<int> > game)
{
	int		i = 0;
	int		j;

	clear();

	for (int f = 0 ; f < y; f++)
		mvprintw(0, f + 1, "_");
	while (i < x)
	{
		j = 0;
		mvprintw(i + 1, 0, "|");
		mvprintw(i + 1, y + 1, "|");
		std::cout << std::endl;
		i++;
	}
	for (int f = 0 ; f < y; f++)
		mvprintw(i + 1 , f + 1, "-");

	std::list<std::vector<int> >::iterator	it;
	std::list<std::vector<int> >::iterator	end = game.end();

	for (it = game.begin(); it != end ; it++)
	{
		if ((*it)[2] == 0)
			mvprintw((*it)[0] + 1 , (*it)[1] + 1, "Q");
		if ((*it)[2] == 1)
			mvprintw((*it)[0] + 1 , (*it)[1] + 1, "o");
		if ((*it)[2] == 2)
			mvprintw((*it)[0] + 1 , (*it)[1] + 1, "@");
	}
}

ANcurses*	createLib()
{
	return new ANcurses();
}

void		deleteLib(INibbler *lib)
{
	endwin();
	delete lib;
}