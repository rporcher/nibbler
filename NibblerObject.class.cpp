#include "NibblerObject.class.hpp"
#include <iostream>
#include <string>
#include <ncurses.h>
#include <cstdlib>

NibblerObject::NibblerObject( int type, int x, int y, int move) : _type(type), _x(x), _y(y), _move(move)
{
	
}


NibblerObject::NibblerObject( void )
{
	
}

NibblerObject::NibblerObject(NibblerObject const & src)
{
	*this = src;

	return ;
}

std::string	NibblerObject::toString() const
{
	return "NibblerObject";
}

NibblerObject::~NibblerObject( void)
{
	
}

NibblerObject &	NibblerObject::operator=(NibblerObject const & rhs)
{
	_type = rhs.getType();
	_x = rhs.getX();
	_y = rhs.getY();
	_move = rhs.getMove();

	return *this;
}

void	NibblerObject::lose(Nibbler *game)
{
	game->getLib()->end(game->getX(), game->getY(), game->gameInfo());
	std::cout << "You lost!" << std::endl;
	std::exit(0);
}

int		NibblerObject::getType() const
{
	return this->_type;
}

void	NibblerObject::setType(int val)
{
	this->_type = val;
}

int		NibblerObject::getX() const
{
	return this->_x;
}

void	NibblerObject::setX(int val)
{
	this->_x = val;
}

int		NibblerObject::getY() const
{
	return this->_y;
}

void	NibblerObject::setY(int val)
{
	this->_y = val;
}

int		NibblerObject::getMove() const
{
	return this->_move;
}

void	NibblerObject::setMove(int val)
{
	this->_move = val;
}


void	NibblerObject::disable()
{
	_type = -1;
}

int		NibblerObject::moveup(Nibbler * game)
{
	NibblerObject*	food;

	if (_x == 0 || game->findObject(_x -1  , _y))
		lose(game);		
	_x--;
	if ((food = game->findFood(_x, _y)))
	{
		food->disable();
		food->initFood(game);
		return (1);
	}
	return (0);
}

int		NibblerObject::moveleft(Nibbler * game)
{
	NibblerObject*	food;

	if (_y == 0 || game->findObject(_x , _y - 1))
		lose(game);
	_y--;
	if ((food = game->findFood(_x, _y)))
	{
		food->disable();
		food->initFood(game);
		return (1);
	}
	return (0);
}

int		NibblerObject::moveright(Nibbler * game)
{
	NibblerObject*	food;

	if (_y == game->getY() - 1 || game->findObject(_x , _y + 1))
		lose(game);
	_y++;
	if ((food = game->findFood(_x, _y)))
	{
		food->disable();
		food->initFood(game);
		return (1);
	}
	return (0);
}

int		NibblerObject::movedown(Nibbler * game)
{
	NibblerObject*	food;
	

	if (_x == game->getX() - 1 || game->findObject(_x + 1, _y))
		lose(game);
	_x++;
	if ((food = game->findFood(_x, _y)))
	{
		food->disable();
		food->initFood(game);
		return (1);
	}
	return (0);
}

void	NibblerObject::initFood(Nibbler * game)
{
	int				nx;
	int				ny;

	while (1)
	{
		nx = std::rand() % game->getX();
		ny = std::rand() % game->getY();
		if (!game->findObject(nx, ny) && !game->findFood(nx,ny))
			break;
	}
	_x = nx;
	_y = ny;
	_type = 2;
}

int		NibblerObject::getNmove() const
{
	return this->_nmove;
}

void	NibblerObject::setNmove(int val)
{
	this->_nmove = val;
}