#include "Nibbler.class.hpp"
#include <iostream>
#include <string>

typedef int (NibblerObject::*MoveFunc)(Nibbler *);

Nibbler::Nibbler( void )
{
	
}

Nibbler::Nibbler(Nibbler const & src)
{
	*this = src;

	return ;
}

std::string	Nibbler::toString() const
{
	return "Nibbler";
}

Nibbler::~Nibbler( void)
{
	
}

void		Nibbler::push(NibblerObject * val)
{
	_list.push_back(val);
}


void		Nibbler::pushFood(NibblerObject * val)
{
	_food.push_back(val);
}


Nibbler &	Nibbler::operator=(Nibbler const &rhs)
{
	_x = rhs.getX();
	_y = rhs.getY();
	_list = rhs.getList();

	return *this;
}

std::list<NibblerObject *>		Nibbler::getList() const
{
	return this->_list;
}

void	Nibbler::setList(std::list<NibblerObject *> &val)
{
	this->_list = val;
}

int		Nibbler::getX() const
{
	return this->_x;
}

void	Nibbler::setX(int &val)
{
	this->_x = val;
}

int		Nibbler::getY() const
{
	return this->_y;
}

void	Nibbler::setY(int &val)
{
	this->_y = val;
}

std::list<NibblerObject *>		Nibbler::getFood() const
{
	return this->_food;
}

void	Nibbler::setFood(std::list<NibblerObject *> &val)
{
	this->_food = val;
}

INibbler*	Nibbler::getLib()
{
	return _lib;
}
void	Nibbler::setLib(INibbler* &val)
{
	_lib = val;
}

NibblerObject 	*Nibbler::findObject(int x, int y)
{
	std::list<NibblerObject*>::iterator	it;
	std::list<NibblerObject*>::iterator	end = _list.end();

	for (it = _list.begin(); it != end ; it++)
	{
		if ((*it)->getType() != -1)
		{
			if (x == (*it)->getX() && y == (*it)->getY())
				return (*it);	
		}
	}
	return NULL;
}

NibblerObject 	*Nibbler::findFood(int x, int y)
{
	std::list<NibblerObject*>::iterator	it;
	std::list<NibblerObject*>::iterator	end = _food.end();

	for (it = _food.begin(); it != end ; it++)
	{
		if ((*it)->getType() != -1)
		{
		if (x == (*it)->getX() && y == (*it)->getY())
			return (*it);	
		}
	}
	return NULL;
}
 
void	Nibbler::move()
{
	MoveFunc	tab[4] = {&NibblerObject::moveup, &NibblerObject::moveright, &NibblerObject::movedown, &NibblerObject::moveleft};
	std::list<NibblerObject*>::iterator	it = _list.begin();
	std::list<NibblerObject*>::iterator	next;
	std::list<NibblerObject*>::iterator	end = _list.end();
	std::list<NibblerObject*>::iterator	end2 = _list.end();

	if (((*it)->*tab[(*it)->getMove()])(this))
	{
		_list.push_back(new NibblerObject(1, _list.back()->getX(), _list.back()->getY(), _list.back()->getMove()));
		end--;
		end2--;
	}
	end--;
	for (it = _list.begin(); it != end ; it++)
	{
		next = it;
		next++;
		((*next)->*tab[(*next)->getMove()])(this);
		(*next)->setNmove((*it)->getMove());
	}
	it = _list.begin();
	for (it++; it != end2 ; it++)
		(*it)->setMove((*it)->getNmove());
}

void	Nibbler::turnLeft()
{
	int		i = _list.front()->getMove();
	if (i == 0)
		i = 3;
	else
		i--;
	_list.front()->setMove(i);
}

void	Nibbler::turnRight()
{
	int		i = _list.front()->getMove();
	if (i == 3)
		i = 0;
	else
		i++;
	_list.front()->setMove(i);
}

std::vector<int>	ft_vectorinfo(NibblerObject* ob)
{
	std::vector<int>	o(3);

	o[0] = ob->getX();
	o[1] = ob->getY();
	o[2] = ob->getType();

	return o;
}

std::list<std::vector<int> >	Nibbler::gameInfo()
{
	std::list<std::vector<int> >		info;
	std::vector<int>	o;

	std::list<NibblerObject*>::iterator	it;
	std::list<NibblerObject*>::iterator	end = _list.end();


	for (it = _list.begin(); it != end ; it++)
	{
		if ((*it)->getType() != -1)
			info.push_back(ft_vectorinfo((*it)));
	}
	end = _food.end();

	for (it = _food.begin(); it != end ; it++)
	{
		if ((*it)->getType() != -1)
			info.push_back(ft_vectorinfo((*it)));
	}
	return info;
}

