#ifndef AOPENGL_HEADER
#define AOPENGL_HEADER
#include <string>
#include <iostream>
#include <map>
#include <unistd.h>
#include <GLFW/glfw3.h>
#include <INibbler.class.hpp>

class AOpengl : public INibbler
{
public:
	AOpengl();
	AOpengl(AOpengl const &);
	virtual ~AOpengl();
	std::string	toString() const;
	AOpengl&	operator=(AOpengl const &);
	void	init();
	char	getInput();
	void	libraryLoop();
	void	printGame(int x, int y, std::list<std::vector<int> > game);
	void	end(int x, int y, std::list<std::vector<int> > game);
	GLFWwindow	*getWin();
	void	setKey(int i, char c);
	
private:
	GLFWwindow			*window;
	std::map<int, char>	_key;

};
	AOpengl*	getOpengl(AOpengl* lib);

extern "C" 
{
	AOpengl		*createLib();
	void		deleteLib(INibbler *lib);
}

std::ostream	&operator<<(std::ostream &o, AOpengl const &i);
#endif /*AOPENGL_HEADER*/