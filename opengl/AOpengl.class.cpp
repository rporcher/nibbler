#include "AOpengl.class.hpp"

static void	key_callback(GLFWwindow *window, int key,
	int scancode, int action, int mods)
{
	/*if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);*/

	(void)window;
	(void)scancode;
	(void)mods;
	if (action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_LEFT)
			getOpengl(NULL)->setKey(key, 'l');
		if (key == GLFW_KEY_RIGHT)
			getOpengl(NULL)->setKey(key, 'r');
		if (key == GLFW_KEY_1)
			getOpengl(NULL)->setKey(key, '1');
		if (key == GLFW_KEY_2)
			getOpengl(NULL)->setKey(key, '2');
	}
}



AOpengl::AOpengl()
{

}
AOpengl::AOpengl(AOpengl const &model)
{
	*this = model;
}
AOpengl::~AOpengl()
{
}
std::string	AOpengl::toString() const
{
	return "AOpengl";
}
AOpengl&	AOpengl::operator=(AOpengl const &copy)
{
	window = copy.window;
	return *this;
}
std::ostream	&operator<<(std::ostream &o, AOpengl const &i)
{
	o << i.toString();
	return o;
}

void	AOpengl::setKey(int i, char c)
{
	_key[i] = c;
}

void	AOpengl::init()
{
	if (!glfwInit())
		exit(EXIT_FAILURE);
	this->window = glfwCreateWindow(640, 640, "Nibbler", NULL, NULL);
	if (!this->window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(this->window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(this->window, key_callback);
	getOpengl(this);
}

char	AOpengl::getInput()
{
	std::map<int, char>::iterator	it;
	std::map<int, char>::iterator	end = this->_key.end();

	for (it = this->_key.begin(); it != end ; it++)
	{
		if ((*it).second != 'n')
		{
			(*it).second = 'n';
			return ((*it).first);
		}
	}
	return 'n';
}

GLFWwindow	*AOpengl::getWin()
{
	return this->window;
}

void	AOpengl::libraryLoop()
{
	float	ratio;
	int		width;
	int		height;

	glfwGetFramebufferSize(this->window, &width, &height);
	ratio = width / (float)height;
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	usleep(10000);
	glfwPollEvents();
}

void	AOpengl::end(int x, int y, std::list<std::vector<int> > game)
{
	(void)x;
	(void)y;
	(void)game;
	glfwWaitEvents();

	glfwDestroyWindow(this->window);
	glfwTerminate();
}

void	AOpengl::printGame(int x, int y, std::list<std::vector<int> > game)
{
	(void)x;
	(void)y;
	(void)game;
	glBegin(GL_TRIANGLES);
        glColor3f(1.f, 0.f, 0.f);
        glVertex3f(-0.6f, -0.4f, 0.f);
        glColor3f(0.f, 1.f, 0.f);
        glVertex3f(0.6f, -0.4f, 0.f);
        glColor3f(0.f, 0.f, 1.f);
        glVertex3f(0.f, 0.6f, 0.f);
        glEnd();
}

AOpengl*	getOpengl(AOpengl *lib)
{
	static AOpengl *	singleton = NULL;

	if (lib)
		singleton = lib;

	return singleton;
}

AOpengl*	createLib()
{
	return new AOpengl();
}

void		deleteLib(INibbler *lib)
{
	glfwDestroyWindow(getOpengl(NULL)->getWin());
	glfwTerminate();

	delete lib;
}

