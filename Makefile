NAME =			nibbler

CC =			g++ $(FLAGS)

SRC =			main.cpp Nibbler.class.cpp NibblerObject.class.cpp

OPENGLFL =		-L./opengl/glfw/src/ -lglfw3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo
OPENGLSRC =		opengl/AOpengl.class.cpp

NIBBLERFL =		-lncurses
NIBBLERSRC	=	ncurse/ANcurses.class.cpp



FLAGS =			-Wall -Wextra -Werror

all:			opengl ncurse $(NAME)

opengl:			
				git submodule init opengl/glfw
				git submodule update opengl/glfw
				cmake opengl/glfw/CMakeLists.txt
				Make -C opengl/glfw
				g++ $(FLAGS) $(OPENGLFL) -shared -fPIC -I ./opengl -I ./opengl/glfw/include -I ./ -o opengl.so $(OPENGLSRC)

ncurse:			
				g++ $(FLAGS) $(NIBBLERFL) -shared -fPIC -I ./ncurse -I ./ -o ncurse.so $(NIBBLERSRC)


$(NAME):		
				@echo "\033[32m[Make]\033[0m   " | tr -d '\n'
				@echo "\033[36m[$(NAME)]\033[0m " | tr -d '\n'
				@echo "Building $(NAME)... " | tr -d '\n'
				$(CC) -o $(NAME) $(SRC) -I ./
				@echo "\033[32m   -> \033[0m" | tr -d '\n'
				@echo "\033[36m$(NAME) \033[0m\033[32mcreated\033[0m"

# clean:
# 				Make -C libft clean
# 				@echo "\033[31m[clean] " | tr -d '\n'
# 				@echo "\033[36m[$(NAME)]\033[0m Remove ofiles"
# 				rm -f $(OBJ)

fclean:
				rm -f $(NAME)
				rm -f opengl.so
				rm -f ncurse.so
				@echo "\033[31m[fclean] \033[36m[$(NAME)]\033[0m" | tr -d '\n'
				@echo " Remove $(NAME)"

re:				fclean all

.PHONY:			all clean re fclean opengl ncurse
.SILENT:
