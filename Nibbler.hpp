#ifndef NIBBLER_HPP
# define NIBBLER_HPP

#include "Nibbler.class.hpp"
#include "NibblerObject.class.hpp"
#include <cstdlib>

class Nibbler;

Nibbler	*ft_gameinit(char **ag);
void	print_grid(Nibbler *game);

#endif
