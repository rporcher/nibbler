#include "Nibbler.hpp"
#include "INibbler.class.hpp"
#include "ncurse/ANCurses.class.hpp"
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include <dlfcn.h>

Nibbler	*ft_gameinit(char **ag)
{
	Nibbler		*game = new Nibbler();
	int			x = std::atoi(ag[2]);
	int			y = std::atoi(ag[1]);

	if (x < 20 || x > 100 || y < 20 || y > 100)
		return NULL;
	game->setX(x);
	game->setY(y);


	NibblerObject	*head = new NibblerObject(0, ((x / 2) - 1), ((y / 2)), 0);
	game->push(head);
	NibblerObject	*tail;
	for (int i = 0 ; i < 3 ; i++)
	{
		tail = new NibblerObject(1, ((x / 2) + i), ((y / 2)), 0);
		game->push(tail);
	}
	tail = new NibblerObject(2, -1 , -1 , 0);
	tail->initFood(game);
	game->pushFood(tail);
	return (game);
}

char		*ft_switchlib(Nibbler *game, char i)
{
	INibbler *	(*libptr)();
	void		(*delptr)(INibbler *lib);
	static void		*dl_handle = NULL;
	INibbler *	ret;

	if (dl_handle)
	{
		delptr = (void(*)(INibbler *)) dlsym(dl_handle, "deleteLib");
		delptr(game->getLib());
		if (dlclose(dl_handle))
			return (dlerror());
	}
	if (i == '1')
		dl_handle = dlopen("ncurse.so", RTLD_LAZY | RTLD_LOCAL);
	else if (i == '2')
		dl_handle = dlopen("opengl.so", RTLD_LAZY | RTLD_LOCAL);
	else
		return (dlerror());
	if (!dl_handle)
		return (dlerror());
	libptr = (INibbler*(*)()) dlsym(dl_handle, "createLib");
	if (!libptr)
		return (dlerror());
	ret = libptr();
	game->setLib(ret);

	return (NULL);
}


int		main(int ac, char **ag)
{
	Nibbler		*game;
	char		*err;
	char		c;

	if (ac != 3 || !(game = ft_gameinit(ag)))
	{
		std::cout << "Usage: nibbler <width>[20~100] <height>[20~100]" << std::endl;
		return (0);
	}
	std::srand(std::time(0));
	
	if (ft_switchlib(game, '2'))
		return (0);

	game->getLib()->init();

	while (1)
	{
		game->getLib()->libraryLoop();
		c  = game->getLib()->getInput();
		if(c == 'l')
			game->turnLeft();
		if(c == 'r')
			game->turnRight();
		if(c == '1' || c == '2')
		{
			if ((err = ft_switchlib(game, c)))
			{
				std::cout << err << std::endl;
				return (0);
			}
		}
		game->move();
		game->getLib()->printGame(game->getX(), game->getY(), game->gameInfo());
	}
	return (0);
}
