#ifndef NIBBLEROBJECT_CLASS_HPP
# define NIBBLEROBJECT_CLASS_HPP

#include <string>
#include <iostream>
#include "Nibbler.hpp"

class Nibbler;

class NibblerObject
{
public:
	NibblerObject(int type, int x, int y, int move);
	NibblerObject(NibblerObject const & src);
	virtual ~NibblerObject();
	NibblerObject &	operator=(NibblerObject const & rhs);
	std::string	toString() const;
	int		getType() const;
	void	setType(int val);
	int		getX() const;
	void	setX(int val);
	int		getY() const;
	void	setY(int val);
	int		getMove() const;
	void	setMove(int val);
	int		moveup(Nibbler * game);
	int		moveright(Nibbler * game);
	int		moveleft(Nibbler * game);
	int		movedown(Nibbler * game);
	void	lose(Nibbler * game);
	void	check_hit(Nibbler *game, int x, int y);
	int		getNmove() const;
	void	setNmove(int val);
	void	disable();
	void	initFood(Nibbler * game);

private:
	NibblerObject();
	int			_type;
	int			_x;
	int			_y;
	int			_move;
	int			_nmove;
};

#endif
